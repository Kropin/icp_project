PACKAGE=xprajk00-xkropa06.zip
GAME=hra2016
CFLAGS=-std=c++11 -Wall -Wextra -pedantic -g
CC=g++
FILES=src/main.o src/game.o src/player.o src/table.o src/moves.o src/state_game.o src/algorithms.o



all: cli gui 


src/main.o: src/main.cpp
	$(CC) $(CFLAGS) -c src/main.cpp -o src/main.o

src/game.o: src/game.cpp
	$(CC) $(CFLAGS) -c src/game.cpp -o src/game.o

src/player.o: src/player.cpp
	$(CC) $(CFLAGS) -c src/player.cpp -o src/player.o

src/table.o: src/table.cpp
	$(CC) $(CFLAGS) -c src/table.cpp -o src/table.o

src/moves.o: src/moves.cpp
	$(CC) $(CFLAGS) -c src/moves.cpp -o src/moves.o

src/state_game.o: src/state_game.cpp
	$(CC) $(CFLAGS) -c src/state_game.cpp -o src/state_game.o

src/algorithms.o: src/algorithms.cpp
	$(CC) $(CFLAGS) -c src/algorithms.cpp -o src/algorithms.o

cli: $(FILES)
	$(CC) $(CFLAGS) $(FILES) -o $(GAME)-cli

gui:
	@cd src && qmake -o Makefile && make
	mv src/$(GAME) ./$(GAME)

clean:
	rm -f src/*.o $(GAME)-cli $(GAME) src/Makefile src/ui_gui.h
	rm -rf doc/

doxygen:
	mkdir -p doc
	@cd src && doxygen doxygen.conf 2>/dev/null 1>/dev/null

pack: clean
	zip -r $(PACKAGE) Makefile src/ 

