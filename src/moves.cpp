/** @file moves.cpp
*   @brief Moves class source file
*   @details File contains implementation of Moves methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "moves.h"

Moves::Moves(int t_size, int ***table): size{t_size}
{
    int half_size = size/2;

    ref_table = table;

    undo_vec.push_back(color());
    undo_vec.back().move = BLACK; 
    undo_vec.back().white.push_back(coords(half_size,half_size));
    undo_vec.back().white.push_back(coords(half_size-1,half_size-1));
    undo_vec.back().black.push_back(coords(half_size-1,half_size));
    undo_vec.back().black.push_back(coords(half_size,half_size-1));
}

void Moves::push_state(int color_)
{
    color_ = color_*(-1);   //ziskani toho kdo ma v budoucnu tahnout

    undo_vec.push_back(color());
    undo_vec.back().move = color_;

    for(int i = 0; i<size; ++i)
    {
        for(int j = 0; j<size; ++j)
        {
            if((*ref_table)[i][j]==WHITE)
                undo_vec.back().white.push_back(coords(i,j));
            else if ((*ref_table)[i][j]==BLACK)
                undo_vec.back().black.push_back(coords(i,j));
        }
    }
}

void Moves::print_vec()
{
    for(auto x = undo_vec.begin(); x!=undo_vec.end(); ++x)
    {
        std::cout << "\nNa tahu: " << x->move << " " << undo_vec.size() << "\n";
        std::cout << "Bile kameny\n";
        for(auto y = x->white.begin(); y!=x->white.end(); ++y)
        {
            std::cout << "x: " << y->x+1 << " y:" << y->y+1 << "\n";
        }
        std::cout << "Cerne kameny\n";
        for(auto t = x->black.begin(); t!=x->black.end(); ++t)
        {
            std::cout << "x: " << t->x+1 << " y:" << t->y+1 << "\n";
        }
    }
    std::cout << "\n\n";
}

void Moves::print_redo()
{
    for(auto x = redo_vec.begin(); x!=redo_vec.end(); ++x)
    {
        std::cout << "\nNa tahu: " << x->move << "\n";
        std::cout << "Bile kameny\n";
        for(auto y = x->white.begin(); y!=x->white.end(); ++y)
        {
            std::cout << "x: " << y->x+1 << " y:" << y->y+1 << "\n";
        }
        std::cout << "Cerne kameny\n";
        for(auto t = x->black.begin(); t!=x->black.end(); ++t)
        {
            std::cout << "x: " << t->x+1 << " y:" << t->y+1 << "\n";
        }
    }
    std::cout << "\n\n";
}

int Moves::undo(int multiplayer)
{
    if(undo_vec.size()<2)
        return undo_vec.back().move;

    if(multiplayer!=0 && undo_vec.size()==2)
        return BLACK;

    clear_table();

    redo_vec.push_back(undo_vec.back());
    undo_vec.pop_back();

    if(multiplayer!=0)
    {
        for(auto x=undo_vec.rbegin(); x!=undo_vec.rend(); ++x)
        {

            if(x->move==1)
                break;
            else
                undo_vec.pop_back();
        }
    }

    color tmp = undo_vec.back();

    set_table_color(tmp);

    return tmp.move;
}

int Moves::redo()
{
    if (redo_vec.empty())
        return undo_vec.back().move;

    clear_table();

    color tmp=redo_vec.back();

    set_table_color(tmp);

    push_state(tmp.move*(-1));
    redo_vec.pop_back();
    return tmp.move;
}

void Moves::clear_redo()
{
    redo_vec.clear();
}


void Moves::clear_table()
{
    for(int i = 0; i<size; ++i)
    {
        for(int j = 0; j<size; ++j)
           (*ref_table)[i][j]=0;
    }
}

void Moves::set_table_color(color tmp)
{
    for(auto x = tmp.white.begin(); x!=tmp.white.end(); ++x)
        (*ref_table)[x->x][x->y]=WHITE;
    for(auto x = tmp.black.begin(); x!=tmp.black.end(); ++x)
        (*ref_table)[x->x][x->y]=BLACK;
}

void Moves::count_coords(int *count_white, int *count_black)
{
    for(int i=0; i<size; ++i)
    {
        for(int j=0; j<size; ++j)
        {
            if((*ref_table)[i][j]==1)
                (*count_black)++;
            else if((*ref_table)[i][j]==-1)
                (*count_white)++;
        }
    }
}

std::vector<color> Moves::get_undo_vec()
{
    return undo_vec;
}

void Moves::setup_undo_vec(std::vector<color> moves)
{
    if(!undo_vec.empty())
        undo_vec.clear();
    for(auto i=moves.begin(); i!=moves.end(); ++i)
        undo_vec.push_back(*i);
    set_table_color(moves.back());
}




