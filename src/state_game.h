/** @file state_game.h
*   @brief State_game class header file
*   @details File contains declaration of State_game methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef STATE_GAME_H
#define STATE_GAME_H

#include "coords.h"
#include "table.h"
#include "moves.h"
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <ctime>



class State_game
{
    struct data {
    	int table_size;
    	int algorithm;
    	bool on_move;
    	std::vector<color> undo_vec;
    };

    data game_data;
    /**
    Parsing curly brackets from the string.
    @param line Line to parse.
    @param *data Pointer to vector where will be stored result.
    @return  0 if the parsing was successful otherwise -1 is returned.
    */
    int parse_curly(std::string line, std::vector<std::string> *data);
    /**
    Parse round brackets with coords of rocks.
    @param data String containing coords in round brackets.
    @param *move Pointer to vector of coords. 
    @return 0 if the parsing was successful otherwise non zero value is returned.
    */
    int parse_bracket(std::string data, std::vector<coords> *move);
    /**
    Split string using specified delimiter.
    @param str String to be splitted.
    @param delimiter Delimiter.
    @return Vector of splitted strings.
    */
    std::vector<std::string> split_string(const std::string& str, const std::string& delimiter);
    /**
    Function returns actual time in format: year-month-day_hour-minute-seconds
    @return String containing formated time.
    */
    std::string current_time();
    /**
    Read whole file content into string.
    @param filename Name of the file to be read.
    @param *data Pointer to string where will be stored readed data.
    @return 0 if the file is readed otherwise -1 is returned.
    */
    int read_file(std::string filename, std::string *data);
    /**
    Write the data into file.
    @param data Data to be written.
    @param time Actual time.
    @return 0 if the data were successfully written into file otherwise -1 is returned.
    */
    int write_to_file(std::string data, std::string time);
    /**
    Function converts string into integer.
    @param value String containing the value to convert.
    @param *data Pointer to where will be the result of conversion stored.
    @return 0 if the conversion was successful otherwise negative value is returned.
    */
    int to_int(std::string value, int *data);


public:

    State_game();
    /**
    Main function for saving actual game state.
    @param algorithm Used algorithm in this game.
    @param on_move Specifies the player who is actual on move.
    @param size Size of the table.
    @param moves Vector of the moves. Saved are only three last moves of each player.
    @return 0 if game was successfully saved, otherwise non zero value is returned.
    */
    int save_game(int algorithm, bool on_move, int size, std::vector<color> moves);
    /**
    Main function for loading the game.
    @param filename Name of the file where is stored game state.
    @return 0 if the loading was successful, otherwise non zero value is returned.
    */
    int load_game(std::string filename);
    /**
    Returns the table size.
    @return Table size.
    */
	int get_table_size();
    /**
    Returns the algorithm used in saved game.
    @return Used algorithm.
    */
	int get_algorithm();
    /**
    Returns the color of the player who were on move in saved game.
    @return Color of the player.
    */
	bool get_on_move();
    /**
    Returns the vector of played moves.
    @return Vector of moves.
    */
	std::vector<color> get_moves();

    void print_struct();



};

#endif // STATE_GAME_H
