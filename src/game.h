/** @file game.h
*   @brief Game class header file
*   @details File contains declaration of Game methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef GAME_H
#define GAME_H

#include "table.h"
#include "player.h"
#include "moves.h"
#include "algorithms.h"
#include "state_game.h"




class Game{
private:
    Player player1;
    Player player2;
public:
    Table main_table;
    /**
    Constructor creates players and table.
    @param size Table size.
    */
    Game(int size);
    /**
    Print table with score.
    */
    void print_table();
    /**
    Main function for playing Reversi in command line interface.
    @param algorithm Specifies the type of the used algorithm - 0 for multiplayer.
    @param load Specifies the game - New game or load saved game.
    */
    void play_cli(int algorithm, int load);
private:
    char alpha[12]={'A','B','C','D','E','F','G','H','I','J','K','L'};
    int get_action(int *x, int *y, int player);


};

#endif // GAME_H
