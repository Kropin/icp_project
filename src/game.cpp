/** @file game.cpp
*   @brief Game class source file
*   @details File contains implementation of Game methods and operators
*   @author Martin Prajka, František Kropáč
*/

#include "game.h"

extern State_game game_state;

Game::Game(int size): player1(BLACK), player2(WHITE), main_table(size) {}

void Game::play_cli(int algorithm, int load)
{
    bool on_move{true};
    int x{0}, y{0}, action{0};
    Moves mov(main_table.get_size(), &main_table.table);
    Algorithm alg(main_table.get_size(), main_table.table);
    if(load==1)
    {
        on_move=game_state.get_on_move();
        mov.setup_undo_vec(game_state.get_moves());
    }

    while(!main_table.full() && (main_table.get_available(player1.get_color()) || main_table.get_available(player2.get_color())))
    {
        player1.setScore(main_table.get_score(player1.get_color()));
        player2.setScore(main_table.get_score(player2.get_color()));


        main_table.set_available();
        print_table();


        if(on_move)
        {
            if(main_table.get_available(player1.get_color()))
            {
                action=get_action(&x, &y, player1.get_color());
                switch(action)
                {
                case 0:
                    print_table();
                    break;
                case 1:
                    if(mov.undo(algorithm)==WHITE)
                        on_move=false;
                    else
                        on_move=true;
                    main_table.set_available();
                    break;
                case 2:
                    if(mov.redo()==WHITE)
                        on_move=false;
                    else
                        on_move=true;
                    main_table.set_available();
                    break;
                case 3:
                    mov.print_redo();
                    break;
                case 4:
                    mov.print_vec();
                    break;
                case 5:
                    if(main_table.set_color(x, y, player1.get_color())==0)
                    {
                        on_move=false;
                        mov.push_state(player1.get_color());
                        mov.clear_redo();
                        continue;
                    }
                    break;
                case 6:
                    game_state.save_game(algorithm, on_move, main_table.get_size(), mov.get_undo_vec());
                    break;
                }
                main_table.set_available();
            }
            else
            {
                on_move=false;
                continue;
            }
        }


        if(!on_move)
        {
            if(main_table.get_available(player2.get_color()))
            {
                if(algorithm==0)
                    action=get_action(&x, &y, player2.get_color());
                else if(algorithm==1)
                    action=alg.random_choice(&x, &y, player2.get_color());
                else if(algorithm==2)
                    action=alg.most_turns(&x, &y, player2.get_color());

                switch(action)
                {
                case 0:
                    print_table();
                    break;
                case 1:
                    if(mov.undo(algorithm)==WHITE)
                        on_move=false;
                    else
                        on_move=true;
                    main_table.set_available();
                    break;
                case 2:
                    if(mov.redo()==WHITE)
                        on_move=false;
                    else
                        on_move=true;
                    main_table.set_available();
                    break;
                case 3:
                    mov.print_redo();
                    break;
                case 4:
                    mov.print_vec();
                    break;
                case 5:
                    if(main_table.set_color(x, y, player2.get_color())==0)
                    {
                        on_move=true;
                        mov.push_state(player2.get_color());
                        mov.clear_redo();
                        continue;
                    }
                    break;
                case 6:

                    game_state.save_game(algorithm, on_move, main_table.get_size(), mov.get_undo_vec());
                    break;

                }
                main_table.set_available();
            }
            else
            {
                on_move=true;
                continue;
            }
        }
    }
    player1.setScore(main_table.get_score(player1.get_color()));
    player2.setScore(main_table.get_score(player2.get_color()));

    print_table();

    if (player1.getScore()>player2.getScore())
        std::cout << "Vyhral hrac1\n";
    else if (player1.getScore()==player2.getScore())
        std::cout << "Remiza\n";
    else
        std::cout << "Vyhral hrac2\n";
}

void Game::print_table()
{
    int size = main_table.get_size();
    std::cout << "\nScore:\nHrac1 " << player1.getScore() << "\nHrac2 " << player2.getScore() << std::endl;
    std::cout.width(3);
    std::cout << " ";
    for(int i = 0; i < size;++i)
    {
        std::cout.width(3);
        std::cout << alpha[i];
    }
    std::cout << std::endl << "   ";
    for(int i = 0; i<size;++i)
        std::cout << "___";
    std::cout << std::endl;
    for(int i = 0; i < size; ++i)
    {
        std::cout.width(2);
        std::cout << i+1 << "|";
        for (int j = 0; j<size;++j)
        {
            std::cout.width(3);
            if(main_table.table[i][j]==2 ||main_table.table[i][j]==-2 || main_table.table[i][j]==3)
                std::cout << 0;
            else
                std::cout << main_table.table[i][j];

        }
        std::cout << std::endl;
    }
}


int Game::get_action(int *x, int *y, int player)
{
    std::string number;
    std::string player_;
    if(player==1)
        player_="Hrac_C_1";
    else
        player_="Hrac_B_-1";


    std::cout << player_ << std::endl;
    std::cout << "Tah           - zadej souradnice\n";
    std::cout << "Undo          - zadej \"undo\"\n";
    std::cout << "Redo          - zadej \"redo\"\n";
    std::cout << "Ulozeni hry   - zadej \"save\"\n";
    std::cout << "Tisk redo_vec - zadej \"predo\"\n";
    std::cout << "Tisk undo_vec - zadej \"pundo\"\n";
    std::cout << "Tisk tabulky  - zadej \"print\"\n";
    std::cout << "Akce: " << std::flush;

    std::cin >> number;

    if(number=="undo")
        return 1;
    else if(number=="redo")
        return 2;
    else if(number=="predo")
        return 3;
    else if(number=="pundo")
        return 4;
    else if(number=="print")
        return 0;
    else if(number=="save")
        return 6;
    else
    {
        if(number.size()==2)
        {
            *x=number[0]-'0';
            *y=number[1]-'A'+1;
            if (*x < 1 || *x>main_table.get_size() || *y < 1 || *y>main_table.get_size())
                ;
            else
            {
                (*x)--;
                (*y)--;
                return 5;
            }
        }
        else if(number.size()==3)
        {
            *x = (number[0] - '0')*10+(number[1] - '0');
            *y = number[2] - 'A' + 1;
            if (*x < 1 || *x>main_table.get_size() || *y < 1 || *y>main_table.get_size())
                ;
            else
            {
                (*x)--;
                (*y)--;
                return 5;
            }

        }
    }

    while(1)
    {
        std::cout << player_ << ": Zadej validni tah: " <<std::flush;
        std::cin >> number;
        if (number.size()==2)
        {
            *x= number[0] - '0';
            *y= number[1] - 'A' + 1;
            if (*x < 1 || *x>main_table.get_size() || *y < 1 || *y>main_table.get_size())
            {
                continue;
            }
            else
            {
                (*x)--;
                (*y)--;
                return 5;
            }
        }
        else if (number.size()==3)
        {
            *x = (number[0] - '0')*10+(number[1] - '0');
            *y = number[2] - 'A' + 1;
            if (*x < 1 || *x>main_table.get_size() || *y < 1 || *y>main_table.get_size())
            {
                continue;
            }
            else
            {
                (*x)--;
                (*y)--;
                return 5;
            }
        }

    }

}
