/** @file player.h
*   @brief Player class header file
*   @details File contains declaration of Player methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>

const int WHITE{-1};
const int BLACK{1};
const int EMPTY{0};
const int AVAILABLE_BLACK{2};
const int AVAILABLE_WHITE{-2};
const int AVAILABLE{3};

class Player{
private:
    int score;
    int color;

public:
	/**
	Constructor creates player with specified color.
	@param color Specifies the color of the player.
	*/
    Player(int color);
    /**
    Returns the player's score.
    @return Player's score.
    */
    int getScore() const;
    /**
    Set player's score.
    @param value Value of the score.
    */
    void setScore(int value);
    /**
	Returns player's color.
	@preturn Player's color.
    */
    int get_color() const;
};

#endif // PLAYER_H
