/** @file coords.h
*   @brief Shared structures header file
*   @details File contains declaration of shared structures
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef COORDS_H
#define COORDS_H

#include <vector>

    /**
    @struct coords
    Structure contains coordinates of the rock. 
    */
    struct coords {
        int x;
        int y;
        coords(int _x, int _y): x{_x}, y{_y} {}
    };
    /**
    @struct color
    Structure contains pair of coordinates vectors and integer value that specifies who is on move.
    @var std::vector<coords> white 
    */
    struct color {
        std::vector<coords> white;
        std::vector<coords> black;
        int move;
    };



#endif // COORDS_H