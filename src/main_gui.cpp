/** @file main_gui.cpp
*   @brief Main source file
*   @details File contains implementation of main function
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "gui.h"
#include <QApplication>



int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    gui w;
    w.show();

    return a.exec();
}
