/** @file table.cpp
*   @brief Table class source file
*   @details File contains implementation of Table methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "table.h"

Table::Table(int size)
{
    if(size==6 || size== 8 || size == 10 || size==12)
    {
        table = new int* [size];
        for(int i = 0;i < size;++i)
            table[i] = new int [size];

        size_table=size;
        //nulovani
        for(int i = 0; i<size; ++i)
            for(int j = 0; j<size; ++j)
                table[i][j]=0;
        //nastaveni kamenu do pole

        int half_size= size/2;
        table[half_size][half_size]=WHITE;
        table[half_size-1][half_size-1]=WHITE;
        table[half_size-1][half_size]=BLACK;
        table[half_size][half_size-1]=BLACK;
        //puvodni dostupnost pro bilou
        table[half_size][half_size-2]=AVAILABLE_WHITE;
        table[half_size+1][half_size-1]=AVAILABLE_WHITE;
        table[half_size-1][half_size+1]=AVAILABLE_WHITE;
        table[half_size-2][half_size]=AVAILABLE_WHITE;
        //puvodni dostupnost pro cernou
        table[half_size-2][half_size-1]=AVAILABLE_BLACK;
        table[half_size-1][half_size-2]=AVAILABLE_BLACK;
        table[half_size+1][half_size]=AVAILABLE_BLACK;
        table[half_size][half_size+1]=AVAILABLE_BLACK;
    }
    else
    {
        throw std::runtime_error("spatna velikost pole");
    }
}
Table::~Table()
{
    for(int i = 0; i<size_table;++i)
        delete [] table[i];
    delete [] table;
}

int Table::set_color(int x, int y, int color)
{
    if (x>(size_table-1) || y>(size_table-1) || x<0 || y<0)
        throw std::runtime_error("Index is out of range!");

    if (color==WHITE)
    {
        if (table[x][y]==AVAILABLE_WHITE || table[x][y]==AVAILABLE)
        {
            table[x][y]=color;
            turn(x,y,color);
        }
        else
            return 1;
    }
    else if (color == BLACK)
    {
        if (table[x][y]==AVAILABLE_BLACK || table[x][y]==AVAILABLE)
        {
            table[x][y]=color;
            turn(x,y,color);
        }
        else
            return 1;
    }
    return 0;
}

void Table::turn(int i,int j,int color)
{
    int x[8]={-1,-1,-1,0,1,1,1,0};
    int y[8]={-1,0,1,1,1,0,-1,-1};
    int k,l;
    bool check;

    for(int n = 0; n < 8;++n)
    {
        k = i + (x[n]);
        l = j + (y[n]);
        check = false;

        while(!(k>(size_table-1) || l>(size_table-1) || k<0 || l<0) && table[k][l]==(color*(-1)))
        {
            k = k + (x[n]);
            l = l + (y[n]);
            check = true;
        }

        if (!(k>(size_table-1) || l>(size_table-1) || k<0 || l<0))
        {
            if(check && table[k][l]==color)
            {
                k = i + (x[n]);
                l = j + (y[n]);
               while(table[k][l]==(color*(-1)))
               {
                   table[k][l]=color;
                   k = k + (x[n]);
                   l = l + (y[n]);
               }
            }
        }
    }
}

int Table::get_color(int x, int y)
{
    if (x>size_table || y>size_table || x<0 || y<0)
        throw std::runtime_error("Index is out of range!");
    return table[x][y];
}

void Table::clear_table()
{
    for(int i = 0; i < size_table; ++i)
    {
        for(int j = 0; j< size_table;++j)
        {
            if (!(table[i][j]==1 || table[i][j]==-1))
                table[i][j]=0;
        }
    }
}

void Table::set_available()
{
    int x[8]={-1,-1,-1,0,1,1,1,0};
    int y[8]={-1,0,1,1,1,0,-1,-1};
    bool check;
    int k;
    int l;

    this->clear_table();
    for(int i = 0; i < size_table;++i)
    {
        for(int j = 0; j<size_table;++j)
        {
            if(table[i][j]==1 || table[i][j]==-1)
            {
                for(int n = 0; n < 8;++n)
                {
                    k = i + (x[n]);
                    l = j + (y[n]);
                    check = false;

                    while(!(k>(size_table-1) || l>(size_table-1) || k<0 || l<0) && table[k][l]==(table[i][j]*(-1)))
                    {
                        k = k + (x[n]);
                        l = l + (y[n]);
                        check = true;
                    }
                    if (!(k>(size_table-1) || l>(size_table-1) || k<0 || l<0))
                    {
                        if(check && (table[k][l]==0 || table[k][l]==3 || table[k][l]==-2 || table[k][l]==2))
                        {
                            if (table[k][l]==0)
                                table[k][l]=table[i][j]*2;
                            else if (table[i][j]*2 == table[k][l])
                                table[k][l]=table[i][j]*2;
                            else
                                table[k][l]=3;
                        }
                    }
                }
            }
        }
    }
}

int Table::get_score(int color)
{
    int score = 0;
    for(int i = 0; i < size_table; ++i)
    {
        for(int j = 0; j< size_table;++j)
        {
            if (table[i][j]==color)
                score++;
        }
    }
    return score;
}

int Table::get_available(int color)
{
    int available=0;
    for(int i=0; i<size_table; ++i)
    {
        for(int j=0; j<size_table; ++j)
        {
            if(table[i][j]==color*2 || table[i][j]==3)
                available++;
        }
    }
    return available>0 ? available : 0;
}

int Table::get_size() const
{
    return size_table;
}

int Table::full()
{
    int full=0;
    for(int i = 0;i<size_table;++i)
    {
        for(int j = 0;j<size_table;++j)
        {
            if(table[i][j]==1 || table[i][j]==-1)
                full++;
        }
    }
    return full==(size_table*size_table) ? 1 : 0;
}

//void Table::set_table()