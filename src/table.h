/** @file table.h
*   @brief Table class header file
*   @details File contains declaration of Table methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef TABLE_H
#define TABLE_H
#include <iostream>
#include <stdexcept>
#include "player.h"

class Table {
    int size_table;

public:
    int **table;
    /**
    Constructor creates table with specified size.
    @param size Specified size of the table.
    */
    Table(int size);
    ~Table();
    /**
    Set the rock color on specified coordinates.
    @param x X coordinate.
    @param y Y coordinate.
    @param color New color of the rock.
    @return 0 if the color setup was successful, otherwise 1 is returned.
    */
    int set_color(int x, int y, int color);
    /**
    Returns the color of the rock on specified coordinates.
    @param x X coordinate.
    @param y Y coordinate.
    @return Color of the rock.
    */
    int get_color(int x, int y);
    /**
    Set each element of the table to zero.
    */
    void clear_table();
    /**
    Function compute the availability for booth players.
    */
    void set_available();
    /**
    Returns availability for specified player (color).
    @param color Player's color.
    @return If there is some availability for the player, positive value is returned, otherwise zero value is returned.*/
    int get_available(int color);
    /**
    Returns table size.
    @return Size of the table.
    */
    int get_size() const;
    /**
    Test if the table is full or not.
    @return 1 if table is full, otherwise 0 is returned.
    */
    int full();
    /**
    Returns score of the specified player.
    @param color Player's color.
    @return Player's score.
    */
    int get_score(int color);
    /**
    Function turns rocks after played move.
    @param i X coordinate.
    @param j Y coordinate.
    */
    void turn(int i, int j,int color);
};

#endif // TABLE_H
