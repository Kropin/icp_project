/** @file item_table.h
*   @brief Item_table class header file
*   @details File contains declaration of Item_table methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef ITEM_TABLE_H
#define ITEM_TABLE_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QPixmap>
#include <QBrush>
#include <QGraphicsSceneMouseEvent>


class Item_table:public QObject, public QGraphicsRectItem {
    Q_OBJECT
    int size_rect;
    struct Coords {
        int x;
        int y;
    };
    Coords coords;
    int *rock;
public:
    /**
     * Constructor initializes size of RectItem
     * @param size
     */
    Item_table(int size);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    /**
     *Set coordinates which these are equivalent with Table
     * @param x
     * @param y
     */
    void set_coords(int x, int y);
    /**
     * Binding RectItem with Table
     * @param rrock Pointer to table number
     */
    void set_reference(int *rrock);
    /**
     * Set picture
     */
    void actualize();
    void get_coords(int *x,int *y);
signals:
    void mouse_press(Item_table *tmp);
};


#endif // ITEM_TABLE_H
