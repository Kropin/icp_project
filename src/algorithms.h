/** @file algorithms.h
*   @brief Algorithm class header file
*   @details File contains declaration of Algorithm methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include "player.h"
#include "table.h"
#include <random>
#include <vector>

class Algorithm
{
	int **table;
	int size;
	
	struct coords {
		int x;
        int y;
        int turn;
        coords(int _x,int _y, int _turn): x{_x}, y{_y}, turn{_turn} {}
    };


public:
	/**
	Constructor of Algorithm
	@param t_size Table size.
	@param **ref_table Double dimensional array.
	*/
	Algorithm(int t_size, int **ref_table);
	/**
	Algorithm that returns random choice of the next move.
	@param 	*x 		Reference to x coordinate.
	@param 	*y 		Reference to y coordinate.
	@param 	color	Specifies the color of the player. 
	@return 5 is returned.
	*/
	int random_choice(int *x, int *y, int color);
	/**
	Algorithm that returns coordinates of the move where will be the biggest count of the rocks turned.
	@param 	*x 		Reference to x coordinate.
	@param 	*y 		Reference to y coordinate.
	@param 	color	Specifies the color of the player. 
	@return 5 is returned.
	*/
	int most_turns(int *x, int *y, int color);
};


#endif // ALGORITHMS_H