/** @file state_game.cpp
*   @brief State_game class source
*   @details File contains implementation of State_game methods and operators
*   @author Martin Prajka, František Kropáč
*/
#include "state_game.h"

State_game::State_game() {}

int State_game::save_game(int algorithm, bool on_move, int size, std::vector<color> moves)
{
    std::string state;
    int player1{0}, player2{0}, on_move_int{0};
    color temp_move=moves.back();
    std::vector<color> temp;    

    on_move_int= on_move? 1: 0;

    state.append("{"+std::to_string(size)+"}{"+std::to_string(algorithm)+"}{"+std::to_string(on_move_int)+"}\n");

    for(auto i=moves.size(); i-- >0; )
    {
        if(player1>=3 && player2>=3)
            break;
        if(moves.at(i).move==1)
            player1++;
        else
            player2++;
        temp.push_back(moves.at(i));
    }

    for(auto i=temp.rbegin(); i!=temp.rend(); ++i)
    {
        state.append("{"+std::to_string(i->move)+"}{");
        for(auto j=i->white.begin(); j!=i->white.end(); ++j)
            state.append("("+std::to_string(j->x)+","+std::to_string(j->y)+")");
        state.append("}{");
        for(auto j=i->black.begin(); j!=i->black.end(); ++j)
            state.append("("+std::to_string(j->x)+","+std::to_string(j->y)+")");
        state.append("}\n");
    }
    
    if((write_to_file(state, current_time()))==-1)
        return -1;
    return 0;
}

int State_game::load_game(std::string filename)
{
    std::string data; 
    std::vector<std::string> v;
    int n{0}, a{0};

    if((n=read_file(filename, &data))==-1)
        return n;
    auto strings=split_string(data, "\n");

    if(strings.back()=="")
        strings.pop_back();

    if((parse_curly(strings[0], &v))==-1)
        return -1;
    if((n=to_int(v[0], &(game_data.table_size)))!=0)
        return n;
    if((n=to_int(v[1], &(game_data.algorithm)))!=0)
        return n;
    game_data.on_move=(v[2]=="1") ? true : false;
    if(!v.empty())
        v.clear();


    for(auto i=strings.begin()+1; i!=strings.end(); ++i)
    {
        if((parse_curly(*i, &v))==-1)
            return -1;
    }
    for(std::size_t j=0; j<v.size(); ++j)
    {
        a=j%3;
        if(a==0)
        {
            game_data.undo_vec.push_back(color());
            if((n=to_int(v[j], &(game_data.undo_vec.back().move)))!=0)
                return n;
        }
        else if(a==1)
        {
            if((n=parse_bracket(v[j], &(game_data.undo_vec.back().white)))!=0)
                return n;
        }
        else if(a==2)
        {
            if((n=parse_bracket(v[j], &(game_data.undo_vec.back().black)))!=0)
                return n;
        }
    }
    return 0;
}

std::vector<std::string> State_game::split_string(const std::string& str, const std::string& delimiter)
{
    std::vector<std::string> strings;

    std::string::size_type pos = 0;
    std::string::size_type prev = 0;
    while ((pos = str.find(delimiter, prev)) != std::string::npos)
    {
        strings.push_back(str.substr(prev, pos-prev));
        prev = pos + 1;
    }

    strings.push_back(str.substr(prev));

    return strings;
}

int State_game::parse_bracket(std::string data, std::vector<coords> *move)
{
    int n{0}, x{0}, y{0};
    std::string::size_type pos1=0;
    std::string::size_type prev=0;
    std::string::size_type pos2=0;

    while(!data.empty())
    {
        pos1=data.find_first_of("(", pos1);
        prev=data.find_first_of(",", pos1+1);
        pos2=data.find_first_of(")", prev+1);
        if(pos1==std::string::npos || prev==std::string::npos || pos2==std::string::npos)
            return -1;
        if((n=to_int(data.substr(pos1+1, prev-pos1-1), &x))!=0)
            return n;
        if((n=to_int(data.substr(prev+1, pos2-prev-1), &y))!=0)
            return n;
        move->push_back(coords(x, y));
        data.erase(0, pos2+1);
    }
    return 0;
}

int State_game::to_int(std::string value, int *data)
{
    std::string::size_type sz;
    try
    {
        *data=std::stoi(value, &sz, 10);
        if(sz!=value.length())
            return -5;
    }
    catch(std::invalid_argument& e)
    {
        return -10;
    }
    catch(std::out_of_range& e)
    {
        return -20;
    }
    return 0;
}

int State_game::parse_curly(std::string line, std::vector<std::string> *data)
{
    std::string::size_type pos=0; 
    std::string::size_type prev=0;
    
    for(int i=0; i<3; ++i)
    {
        pos=line.find_first_of("{", pos);
        prev=line.find_first_of("}", pos+1);
        if(pos==std::string::npos || prev==std::string::npos)
            return -1;
        (*data).push_back(line.substr(pos+1, prev-pos-1));
            
        line.erase(0, prev);
    }
    return 0;
}

std::string State_game::current_time()
{
    time_t _time;
    struct tm *time_struct;
    time(&_time);
    time_struct = localtime(&_time);
    char buf[100]={0};

    strftime(buf,sizeof(buf),"%Y-%m-%d_%H-%M-%S",time_struct);
    std::string str(buf);
    return str;
}

int State_game::read_file(std::string filename, std::string *data)
{
    std::ifstream input(filename, std::ios::in);
    if(input.good())
    {
        input.seekg(0, std::ios::end);
        auto size=input.tellg();
        std::string content(size, ' ');
        input.seekg(0);
        input.read(&content[0], size);
        *data=content;
        return 0;
    }
    else
        return -1;
}

int State_game::write_to_file(std::string data, std::string time)
{
    std::string filename("saved_game-"+time+".data");
    std::ofstream output;
    output.open(filename, std::ofstream::out);
    if(!output)
        return -1;
    output << data;
    output.close();
    return 0;
}

int State_game::get_table_size()
{
    return game_data.table_size;
}

int State_game::get_algorithm()
{
    return game_data.algorithm;
}

bool State_game::get_on_move()
{
    return game_data.on_move;
}

std::vector<color> State_game::get_moves()
{
    return game_data.undo_vec;
}

void State_game::print_struct()
{
    std::cout << "Data\n";
    std::cout << "table_size: " <<game_data.table_size << "\nalgorithm: " << game_data.algorithm << std::endl;
    std::cout << "On_move " << game_data.on_move <<std::endl;
    std::cout << "Print undo_vec\n";

    for(auto i=game_data.undo_vec.begin(); i!=game_data.undo_vec.end(); ++i)
    {
        std::cout << "{" << i->move << "}{";
        for(auto j=i->white.begin(); j!=i->white.end(); ++j)
            std::cout << "(" << j->x << "," <<j->y <<")";
        std::cout << "}{";
        for(auto j=i->black.begin(); j!=i->black.end(); ++j)
            std::cout <<"(" <<j->x <<","<< j->y << ")";
        std::cout <<"}\n";
    }

}
