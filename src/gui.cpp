/** @file gui.cpp
*   @brief gui class source file
*   @details File contains implementation of gui methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "gui.h"
#include "ui_gui.h"


gui::gui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::gui)
{
    setFixedSize(570,400);
    setStyleSheet("QPushButton {background: 'white'}; QLCDNumber {background: 'white'};");
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    scene = new QGraphicsScene;
    ui->graphicsView->setStyleSheet("background-color: rgb(0, 48, 0);");
    
    ui->lplayer2->setStyleSheet("color: white;");
    ui->treeView->setStyleSheet("background-color: white;");
    ui->treeView->setStyleSheet("color: black;");
    ui->stackedWidget->setStyleSheet("background-image: url(:/images/background.png);");
    ui->lplaye1->setText("> Player 1");
    player1 = new Player(BLACK);
    player2 = new Player(WHITE);
    state_game = new State_game();
    model_files = new QDirModel(this);
    main_table=nullptr;
    end=false;
    kind_of_game=0;
}

gui::~gui()
{
    if (main_table!=nullptr)
    {
        for(unsigned i = 0; i<vec_items.size();++i)
        {
            Item_table *tmp = vec_items[i];
            delete tmp;
        }
        delete main_table;
    }
    if (kind_of_game!=0)
    {
        delete algorithm;
    }
    delete model_files;
    delete state_game;
    delete player1;
    delete player2;
    delete scene;
    delete ui;
}


void gui::on_bnew_game_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setFixedSize(370,350);
    scene->setSceneRect(0,0,360,340);
    kind_of_game = ui->comboBox->currentIndex();
    unsigned size=0;
    int x=0;
    int wid_hgt=0;
    int y=0;

    if(ui->radio6->isChecked())
    {
        wid_hgt=55;
        size=6;
    }
    else if (ui->radio8->isChecked())
    {
        wid_hgt=42;
        size=8;
    }
    else if (ui->radio10->isChecked())
    {
        wid_hgt=32;
        size=10;
    }
    else if (ui->radio12->isChecked())
    {
        wid_hgt=28;
        size=12;
    }
    main_table = new Table(size);
    mov = new Moves(main_table->get_size(),&(main_table->table));

    if (kind_of_game!=0)
    {
        algorithm = new Algorithm(size,main_table->table);
    }


    int c_x=0;
    int c_y=0;

    for(unsigned i = 0; i<size*size;++i)
    {
        if((i%size)==0 && (i!=0))
        {
            c_x++;
            c_y=0;
        }
        vec_items.push_back(new Item_table(wid_hgt));
        vec_items.back()->set_reference(&(main_table->table[c_x][c_y]));
        connect(vec_items.back(),&Item_table::mouse_press,this,&gui::on_mouse_press);
        c_y++;
    }

    c_x=0;
    c_y=0;


    for(unsigned i = 0; i<vec_items.size();++i)
    {
        if((i%size)==0 && (i!=0))
        {
            y+=wid_hgt;
            x=0;
            c_x++;
            c_y=0;
        }
        Item_table *tmp = vec_items[i];
        tmp->set_coords(c_x,c_y);
        tmp->setRect(x,y,wid_hgt,wid_hgt);
        scene->addItem(tmp);
        x+=wid_hgt;
        c_y++;
        tmp->actualize();
    }
    main_table->set_available();
    on_move=true;

    player1->setScore(main_table->get_score(player1->get_color()));
    player2->setScore(main_table->get_score(player2->get_color()));
    ui->lcdscore1->display(player1->getScore());
    ui->lcdscore2->display(player2->getScore());

}

void gui::on_mouse_press(Item_table *tmp)
{
    int x,y;
    tmp->get_coords(&x,&y);

    main_table->set_available();

    //multiplayer je pro kindofgame = 0
    if (kind_of_game==0)
    {
        if (on_move)
        {
            if(main_table->get_available(player1->get_color()))
            {
                if(main_table->set_color(x,y,player1->get_color())==0)
                {
                    on_move=false;
                    mov->push_state(player1->get_color());
                    mov->clear_redo();
                }
            }
            else
                on_move=false;
        }
        else if (!on_move)
        {
            if (main_table->get_available(player2->get_color()))
            {
                if (main_table->set_color(x,y,player2->get_color())==0)
                {
                   on_move=true;
                   mov->push_state(player2->get_color());
                   mov->clear_redo();
                }
            }
            else
               on_move=true;
        }
    }
    else
    {
        //singleplayer
        if (on_move)
        {
            if(main_table->get_available(player1->get_color()))
            {
                if(main_table->set_color(x,y,player1->get_color())==0)
                {
                    on_move=false;
                    mov->push_state(player1->get_color());
                    mov->clear_redo();
                }
            }
            else
                on_move=false;
        }

        main_table->set_available();

        if (on_move)
        {
            if (!main_table->get_available(player1->get_color()))
                on_move=false;
        }
        else if (!on_move)
        {
            if (!main_table->get_available(player2->get_color()))
                on_move=true;
        }

        while(!on_move)
        {
            main_table->set_available();

            if (main_table->get_available(player2->get_color()))
            {
                if (kind_of_game==1) //algoritmus 1
                    algorithm->random_choice(&x,&y,player2->get_color());
                else if (kind_of_game==2) //algoritmus 2
                    algorithm->most_turns(&x,&y,player2->get_color());

                if (main_table->set_color(x,y,player2->get_color())==0)
                {
                    on_move=true;
                    mov->push_state(player2->get_color());
                    mov->clear_redo();
                }

            }
            else
            {
                main_table->set_available();
                on_move=true;
                if (!main_table->get_available(player1->get_color()))
                    break;
            }

            if (!main_table->get_available(player1->get_color()))
                on_move=false;
        }
    }

    //spolecna cast pro vsechny druhy hry
    player1->setScore(main_table->get_score(player1->get_color()));
    player2->setScore(main_table->get_score(player2->get_color()));
    ui->lcdscore1->display(player1->getScore());
    ui->lcdscore2->display(player2->getScore());

    main_table->set_available();
    if (on_move)
    {
        if (!main_table->get_available(player1->get_color()))
            on_move=false;
    }
    else if (!on_move)
    {
        if (!main_table->get_available(player2->get_color()))
            on_move=true;
    }

    if (on_move)
    {
        ui->lplaye1->setText("> Player 1");
        ui->lplayer2->setText("Player 2");
    }
    else
    {
        ui->lplaye1->setText("Player 1");
        ui->lplayer2->setText("> Player 2");
    }

    actualize_scene();

    if (end_game())
    {
        if (player1->getScore()>player2->getScore())
        {
            QMessageBox::information(this,"Game Over","Player 1: WIN!");
        }
        else if (player1->getScore()<player2->getScore())
        {
            QMessageBox::information(this,"Game Over","Player 2: WIN!");
        }
        else
        {
            QMessageBox::information(this,"Game Over","DRAW!");
        }
        end=true;
    }
}

void gui::actualize_scene()
{
    for(unsigned i = 0; i < vec_items.size();++i)
    {
        vec_items[i]->actualize();
    }
}


void gui::on_bundo_clicked()
{
    if (!end)
    {
        if (mov->undo(kind_of_game)==WHITE)
        {
            on_move=false;
        }
        else
        {
            on_move=true;
        }

        player1->setScore(main_table->get_score(player1->get_color()));
        player2->setScore(main_table->get_score(player2->get_color()));
        ui->lcdscore1->display(player1->getScore());
        ui->lcdscore2->display(player2->getScore());

        if (on_move)
        {
            ui->lplaye1->setText("> Player 1");
            ui->lplayer2->setText("Player 2");
        }
        else
        {
            ui->lplaye1->setText("Player 1");
            ui->lplayer2->setText("> Player 2");
        }

        actualize_scene();
        main_table->set_available();
    }
}

void gui::on_bredo_clicked()
{
    if (!end)
    {
        if (mov->redo()==WHITE)
        {
            on_move=false;
        }
        else
        {
            on_move=true;
        }

        player1->setScore(main_table->get_score(player1->get_color()));
        player2->setScore(main_table->get_score(player2->get_color()));
        ui->lcdscore1->display(player1->getScore());
        ui->lcdscore2->display(player2->getScore());

        if (on_move)
        {
            ui->lplaye1->setText("> Player 1");
            ui->lplayer2->setText("Player 2");
        }
        else
        {
            ui->lplaye1->setText("Player 1");
            ui->lplayer2->setText("> Player 2");
        }

        actualize_scene();
        main_table->set_available();
    }
}


bool gui::end_game()
{
    main_table->set_available();
    if(main_table->full())
        return true;

    if(((main_table->get_available(player1->get_color()))==0) && ((main_table->get_available(player2->get_color()))==0))
        return true;
    return false;
}

void gui::on_bload_game_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);

    model_files->setReadOnly(true);
    model_files->setSorting(QDir::DirsFirst | QDir::IgnoreCase | QDir::Name);

    ui->treeView->setModel(model_files);

    QModelIndex index = model_files->index("./");

    ui->treeView->expand(index);
    ui->treeView->scrollTo(index);
    ui->treeView->setCurrentIndex(index);
    ui->treeView->resizeColumnToContents(0);

}

void gui::on_bsave_game_clicked()
{
    int return_val = state_game->save_game(kind_of_game, on_move, main_table->get_size(), mov->get_undo_vec());
    if (return_val != 0)
    {
        QMessageBox::critical(this,"Error Save","Somethings wrong with save");
        close();
    }
}

void gui::on_bload2_clicked()
{
    QString file_name;
    QModelIndex index = ui->treeView->currentIndex();
    if (!index.isValid())
        return;

    if (model_files->fileInfo(index).isFile())
    {
        file_name = model_files->fileInfo(index).fileName();
    }

    if ((state_game->load_game(file_name.toStdString()))!=0)
    {
        QMessageBox::critical(this,"Error Load","Somethings wrong with load");
        close();
    }

    ui->stackedWidget->setCurrentIndex(1);

    main_table = new Table(state_game->get_table_size());
    mov = new Moves(main_table->get_size(),&(main_table->table));
    kind_of_game = state_game->get_algorithm();
    on_move = state_game->get_on_move();
    mov->setup_undo_vec(state_game->get_moves());

    unsigned size = main_table->get_size();

    if (kind_of_game!=0)
    {
        algorithm = new Algorithm(size,main_table->table);
    }

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setFixedSize(370,350);
    scene->setSceneRect(0,0,360,340);

    int x{0},y{0},wid_hgt{0};

    if (size==6)
        wid_hgt=55;
    else if (size==8)
        wid_hgt=42;
    else if (size==10)
        wid_hgt=32;
    else if (size==12)
        wid_hgt=28;
    else
    {
        QMessageBox::critical(this,"Error Load","Wrong size table");
        close();
    }

    int c_x=0;
    int c_y=0;

    for(unsigned i = 0; i<size*size;++i)
    {
        if((i%size)==0 && (i!=0))
        {
            c_x++;
            c_y=0;
        }
        vec_items.push_back(new Item_table(wid_hgt));
        vec_items.back()->set_reference(&(main_table->table[c_x][c_y]));
        connect(vec_items.back(),&Item_table::mouse_press,this,&gui::on_mouse_press);
        c_y++;
    }

    c_x=0;
    c_y=0;


    for(unsigned i = 0; i<vec_items.size();++i)
    {
        if((i%size)==0 && (i!=0))
        {
            y+=wid_hgt;
            x=0;
            c_x++;
            c_y=0;
        }
        Item_table *tmp = vec_items[i];
        tmp->set_coords(c_x,c_y);
        tmp->setRect(x,y,wid_hgt,wid_hgt);
        scene->addItem(tmp);
        x+=wid_hgt;
        c_y++;
        tmp->actualize();
    }
    main_table->set_available();

    player1->setScore(main_table->get_score(player1->get_color()));
    player2->setScore(main_table->get_score(player2->get_color()));
    ui->lcdscore1->display(player1->getScore());
    ui->lcdscore2->display(player2->getScore());

    if (on_move)
    {
        ui->lplaye1->setText("> Player 1");
        ui->lplayer2->setText("Player 2");
    }
    else
    {
        ui->lplaye1->setText("Player 1");
        ui->lplayer2->setText("> Player 2");
    }
}
