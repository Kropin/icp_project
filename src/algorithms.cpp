/** @file algorithms.cpp
*   @brief Algorithm class source file
*   @details File contains implementation of Algorithm methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "algorithms.h"

Algorithm::Algorithm(int t_size, int **ref_table): size{t_size} 
{
	table=ref_table;
}

int Algorithm::random_choice(int *x, int *y, int color)
{
	std::vector<coords> moves;
	int move{0};

	for(int i=0; i<size; ++i)
	{
		for(int j=0; j<size; ++j)
		{
			if(table[i][j]==2*color || table[i][j]==3)
				moves.push_back(coords(i, j, 0));
		}
	}
	std::random_device random;
	move=random() % moves.size();
	*x=moves[move].x;
	*y=moves[move].y;

	return 5;
}

int Algorithm::most_turns(int *x, int *y, int color)
{
	std::vector<coords> moves;
	int actual_turn{0}, final_turn{0}, k{0}, l{0};
	int xr[8]={-1,-1,-1,0,1,1,1,0};
    int yr[8]={-1,0,1,1,1,0,-1,-1};
    std::size_t move{0};


	for(int i=0; i<size; ++i)
	{
		for(int j=0; j<size; ++j)
		{
			if(table[i][j]==2*color || table[i][j]==3)
			{
				actual_turn=final_turn=0;
				for(int n=0; n<8; ++n)
				{
					k=i+(xr[n]);
					l=j+(yr[n]);
					actual_turn=0;

					while(!(k>(size-1) || l>(size-1) || k<0 || l<0) && table[k][l]==(color*(-1)))
			        {
			            k = k + (xr[n]);
			            l = l + (yr[n]);
			            actual_turn++;
			        }
			        final_turn+=actual_turn;
				}
				moves.push_back(coords(i, j, final_turn));
			}

		}
	}

	final_turn=moves[move].turn;
	*x=moves[move].x;
	*y=moves[move].y;
	for(std::size_t q=0; q<moves.size(); ++q)
	{
		if(final_turn<=moves[q].turn)
		{
			final_turn=moves[q].turn;
			move=q;
		}
	}

	*x=moves[move].x;
	*y=moves[move].y;

	return 5;
}