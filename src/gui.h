/** @file gui.h
 *  @brief gui class header file
 *  @details File contains declaration of gui methods and operators
 *  @author Martin Prajka
 *  @author František Kropáč
 */
#ifndef GUI_H
#define GUI_H

#include <QWidget>
#include <QGraphicsScene>
#include "item_table.h"
#include <vector>
#include "table.h"
#include "player.h"
#include "moves.h"
#include <QMessageBox>
#include "algorithms.h"
#include "state_game.h"
#include <QDirModel>
#include <QDebug>

namespace Ui {
class gui;
}

class gui : public QWidget
{
    Q_OBJECT

public:
    explicit gui(QWidget *parent = 0);
    /**
     * Draw scene
     */
    void actualize_scene();
    /**
     * Check whether the game has finished
     * @return true if finished or false
     */
    bool end_game();
    ~gui();



private slots:
    void on_bnew_game_clicked();
    void on_mouse_press(Item_table *tmp);
    void on_bundo_clicked();
    void on_bredo_clicked();

    void on_bload_game_clicked();

    void on_bsave_game_clicked();

    void on_bload2_clicked();

private:
    Ui::gui *ui;
    QGraphicsScene *scene;
    std::vector<Item_table *> vec_items;
    Table *main_table;
    Player *player1;
    Player *player2;
    bool on_move;
    Moves *mov;
    bool end;
    int kind_of_game;
    Algorithm *algorithm;
    State_game *state_game;
    QDirModel *model_files;


};

#endif // GUI_H
