/** @file moves.h
*   @brief Moves class header file
*   @details File contains declaration of Moves methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#ifndef MOVES_H
#define MOVES_H


#include <utility>
#include "player.h"
#include "coords.h"





class Moves
{

    std::vector<color> undo_vec;
    std::vector<color> redo_vec;
    int size;
    int ***ref_table;
public:

    /**
    Constructor creates and initializes the vector for saving moves.
    @param t_size Size of the board.
    @param ***table Pointer to a double dimensional array.
    */
    Moves(int t_size, int ***table);
    /**
    Push the actual state of table to vector.
    @param color_ Color of the player who will play next move.
    */
    void push_state(int color_);
    void print_vec();
    /**
    Undo last move.
    @param multiplayer Specifies the type of the game - singleplayer or multiplayer
    @return Color of the player who is on move.
    */
    int undo(int multiplayer);
    /**
    Redo last move.
    @return Color of the player who did the move.
    */
    int redo();
    /**
    Clear the redo vector.
    */
    void clear_redo();
    void print_redo();
    /**
    Set whole table to zeros.
    */
    void clear_table();
    /**
    Set up the table on the last move.
    @param tmp Structure with coords of the last move.
    */
    void set_table_color(color tmp);
    /**
    */
    void count_coords(int *count_white, int *count_black);
    /**
    Returns undo vector.
    @return vector of moves.
    */
    std::vector<color> get_undo_vec();
    /**
    Set up undo vector after loading game.
    @param moves Vector of moves.
    */
    void setup_undo_vec(std::vector<color> moves);
};

#endif // MOVES_H
