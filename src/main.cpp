/** @file main.cpp
*   @brief Main source file
*   @details File contains implementation of main function.
*   @author Martin Prajka
*   @author František Kropáč
*/
#include <iostream>
#include <stdexcept>
#include <exception>
#include <sstream>
#include <string>
#include "game.h"
#include "state_game.h"

using std::string;
using std::cout;

int read_line(int implicit);

State_game game_state;

int main(int argc, char **argv)
{
    try
    {
        int size{8}, action{0}, algorithm{0}, load{0};
        string input;
        if(argc==3)
        {
            if(string(argv[1])!="--load")
                throw std::runtime_error("Argument parsing failed.");
            
            if((game_state.load_game(string(argv[2])))!=0)
                throw std::runtime_error("Loading game failed.");
            size=game_state.get_table_size();
            if(!(size!=6 || size!=8 || size!=10 || size!=12))
                throw std::runtime_error("Loading: Size of the table is not valid.");
            algorithm=game_state.get_algorithm();
            if(!(algorithm!=0 || algorithm!=1 || algorithm!=2))
                throw std::runtime_error("Loading: Choosen algorithm is not valid.");

            load=1;
        }
        else if(argc==1)
        {
            cout << "Zadejte velikost hraci desky (implicitni velikost je 8): ";
            if((size=read_line(8))==-1)
                throw std::runtime_error("Spatny format, mourovane kotatko zemrelo.");
            cout << "Menu\nMultiplayer - 1 (implicitne)\nSingleplayer - 2\n";
            if((action=read_line(1))==-1)
                throw std::runtime_error("Spatny format, bile kotatko zemrelo.");
            if(!(action==1 || action==2))
                throw std::runtime_error("Spatna volba. cerne kotatko zemrelo.");

            if(action==2)
            {
                cout << "Vyber algoritmu:\n1 - Algoritmus voli nahodne\n2 - Algoritmus zvoli tah s nejvetsim otoceni kamenu\n";
                if((algorithm=read_line(-1))==-1)
                    throw std::runtime_error("Spatny format, britske kotatko zemrelo.");
                if(!(algorithm==1 || algorithm==2))
                throw std::runtime_error("Spatna volba. perske kotatko zemrelo.");
            }
        }
        else
            throw std::runtime_error("Parsing arguments failed.");
        Game game(size);
        game.play_cli(algorithm, load);
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}

int read_line(int implicit)
{
    string input;
    int action{-1};
    std::getline(std::cin, input);
    if(!input.empty())
    {
        std::istringstream stream(input);
        stream >> action;
        if(action==0)
            action=-1;
    }
    else
        action=implicit;

    return action;
}