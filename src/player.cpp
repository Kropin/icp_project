/** @file player.cpp
*   @brief Player class source file
*   @details File contains implementation of Player methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "player.h"

int Player::getScore() const
{
    return score;
}

void Player::setScore(int value)
{
    score = value;
}

Player::Player(int color)
{
    this->color=color;
    score=2;
}

int Player::get_color() const
{
    return color;
}
