/** @file item_table.cpp
*   @brief Item_table class source file
*   @details File contains implementation of Item_table methods and operators
*   @author Martin Prajka
*   @author František Kropáč
*/
#include "item_table.h"

Item_table::Item_table(int size): size_rect(size)
{
    QPixmap tmp(":/images/empty.png");
    QPixmap empty = tmp.scaled(size,size);
    setBrush(QBrush(empty));

}

void Item_table::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() == Qt::LeftButton)
        emit mouse_press(this);
}
void Item_table::set_coords(int x, int y)
{
    coords.x=x;
    coords.y=y;
}

void Item_table::set_reference(int *rrock)
{
    rock=rrock;
}

void Item_table::actualize()
{
    if (*rock==1)
    {
        QPixmap tmp(":/images/black.png");
        QPixmap black = tmp.scaled(size_rect,size_rect);
        setBrush(QBrush(black));
    }
    else if (*rock==-1)
    {
        QPixmap tmp(":/images/white.png");
        QPixmap white = tmp.scaled(size_rect,size_rect);
        setBrush(QBrush(white));
    }
    else
    {
        QPixmap tmp(":/images/empty.png");
        QPixmap empty = tmp.scaled(size_rect,size_rect);
        setBrush(QBrush(empty));
    }
}

void Item_table::get_coords(int *x,int *y)
{
    *x=coords.x;
    *y=coords.y;
}
