#-------------------------------------------------
#
# Project created by QtCreator 2016-04-20T21:11:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = hra2016
TEMPLATE = app


SOURCES +=\
        gui.cpp \
    item_table.cpp \
    table.cpp \
    player.cpp \
    moves.cpp \
    algorithms.cpp \
    main_gui.cpp \
    state_game.cpp

HEADERS  += gui.h \
    item_table.h \
    table.h \
    player.h \
    moves.h \
    algorithms.h \
    coords.h \
    state_game.h

FORMS    += gui.ui

RESOURCES += \
    images.qrc
